require 'rails_helper'

describe WelcomeController do

  describe "#index" do

    it "renders a proper template" do
      get :index

      expect(response).to render_template :index
    end

  end

end
