FactoryGirl.define do

  factory :account do
    sequence(:email)  { |n| "email#{n}@example.com" }
    password          'secret'

    transient do
      confirm_account true
    end

    after(:create) do |u, evaluator|
      u.confirm if evaluator.confirm_account
    end
  end
end
