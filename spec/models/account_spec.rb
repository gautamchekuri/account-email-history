require 'rails_helper'

describe Account do

  context "Tracking an account's email addresses" do

    it "should store previous email address when an updated email address is confirmed" do

      an_account = Account.new
      emails = [
        "test.user.1@yopmail.com",
        "test.user.2@yopmail.com",
        "test.user.3@yopmail.com",
        "test.user.4@yopmail.com",
        "test.user.5@yopmail.com",
      ]
      #assert_not an_account.valid?
      assert an_account.valid? == false
      an_account.attributes = {
        email: emails.first,
        password: "letmein123",
        password_confirmation: "letmein123"
      }

      assert an_account.valid?
      an_account.save!
      assert an_account.account_emails.count == 0
      assert an_account.email == emails.first

      an_account.email = emails[1]

      an_account.save!
      assert an_account.unconfirmed_email == emails[1]
      assert an_account.email == emails.first
      an_account.account_emails.count == 0

      an_account.confirm
      assert an_account.email == emails[1]
      assert an_account.account_emails.count == 1
      assert an_account.account_emails.first.email == emails.first

      2.upto(emails.length-1) do |ix|

        an_account.email = emails[ix]

        an_account.save!
        assert an_account.unconfirmed_email == emails[ix]
        assert an_account.email == emails[ix-1]
        assert an_account.account_emails.count == ix-1
        
        an_account.confirm
        assert an_account.email == emails[ix]
        assert an_account.account_emails.count == ix
        assert an_account.account_emails.last.email == emails[ix-1]
      end

      #puts an_account.account_emails.order("created_at ASC").map(&:email)
      assert an_account.account_emails.order("created_at ASC").pluck(:email) == emails[0 .. emails.length-2]
      assert an_account.account_emails.last.email == emails[emails.length-2]
    end

  end

end
