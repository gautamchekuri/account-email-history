class CreateAccountEmailsHistory < ActiveRecord::Migration
  def change
    create_table :account_emails_history do |t|
      t.integer :account_id
      t.string :email

      t.timestamps null: false
    end
  end
end
