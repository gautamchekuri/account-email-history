class Account < ActiveRecord::Base
  
  devise :confirmable, :database_authenticatable, :lockable,
         :recoverable, :registerable, :rememberable, :validatable
         

  has_many :account_emails, class_name: 'AccountEmailsHistory'

  after_update :track_email_updates

  private

  def track_email_updates
    if self.email_changed?
      self.account_emails.create(email: self.email_change.first)
    end
  end

end
