class AccountEmailsHistory < ActiveRecord::Base

  self.table_name = 'account_emails_history'
  
  belongs_to :account

  validates :email, :account_id, presence: true

end
