# backend.challenge

This repository holds the backend development challenge for applicants of Babbel.

## The application

The application in this repository is simplistic. It consists of one model `Account`. The `Account` model is also baked by [devise](https://github.com/plataformatec/devise).

## The task

The customer service department wants to keep track of all the email addresses a user has had in its history as a Babbel customer. The previous email addresses should be persisted.


## Solution

A new model named: AccountEmailsHistory has been added.  
An :account has_many :account_emails.  
account_emails are stored in the account_emails_history table when an account's email address is updated and confrimed.  

To get the previous (most recently updated email address):
``` 
an_account.account_emails.last.email
```

To get the current email address:
```
an_accout.email
```

refer to rspec test case here: spec/models/account_spec.rb


## To setup:  

```
bundle install
rake db:migrate
```

## To run spec:

```
rake spec spec/models/account_spec.rb
```
